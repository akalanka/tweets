package com.example.util;

import com.parse.ParseUser;

public final class ParseUtil {

    public static String getCurrentUsersUserName() {
        ParseUser user = ParseUser.getCurrentUser();

        return user.getUsername();
    }

    private ParseUtil() {
    }
}
