package com.example.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public final class ActivityUtil {

    public static void showAlertDialog(Context context, String title, String message, boolean cancelable) {
        AlertDialog.Builder dialogBuilder = createAlertDialogBuilder(context, title, message, cancelable);

        showDialog(dialogBuilder);
    }

    public static void showAlertDialogWithPositiveButton(Context context, String title, String message, boolean cancelable) {
        AlertDialog.Builder dialogBuilder = createAlertDialogBuilder(context, title, message, cancelable);
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        showDialog(dialogBuilder);
    }

    public static void showAlertDialogForRequiredFields(Context context, String message, boolean cancelable) {
        AlertDialog.Builder dialogBuilder = createAlertDialogBuilder(context, "Missing Required Fields", message, cancelable);
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        showDialog(dialogBuilder);
    }

    private static AlertDialog.Builder createAlertDialogBuilder(Context context, String title, String message, boolean cancelable) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(title);
        dialogBuilder.setMessage(message);
        dialogBuilder.setCancelable(cancelable);

        return dialogBuilder;
    }

    private static void showDialog(AlertDialog.Builder dialogBuilder) {
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private ActivityUtil() {
    }
}
