package com.example.util;

import android.widget.EditText;

public final class CommonUtil {

    private static final String EMPTY_STRING = "";

    public static boolean isNotEmpty(EditText editTextItem) {
        return !(EMPTY_STRING.equals(editTextItem.getText().toString()));
    }

    private CommonUtil() {
    }
}
