package com.example.Tweets;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.util.ActivityUtil;
import com.example.util.ParseUtil;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class TweetActivity extends Activity {

    private EditText newStatusEditText;
    private Button postButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet);

        initializeComponents();
    }

    private void initializeComponents() {
        newStatusEditText = (EditText) findViewById(R.id.newStatusEditText);

        postButton = (Button) findViewById(R.id.addStatusButton);
        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newTweet = newStatusEditText.getText().toString();

                if (!newTweet.isEmpty()) {
                    ParseObject tweetObject = new ParseObject("Tweet");
                    tweetObject.put(getString(R.string.parse_tweet), newTweet);
                    tweetObject.put(getString(R.string.parse_user), ParseUtil.getCurrentUsersUserName());
                    tweetObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Toast.makeText(TweetActivity.this, R.string.tweetSaveSuccess, Toast.LENGTH_SHORT).show();

                                Intent homeIntent = new Intent(TweetActivity.this, HomeActivity.class);
                                startActivity(homeIntent);
                            } else {
                                ActivityUtil.showAlertDialogWithPositiveButton(TweetActivity.this, getString(R.string.tweetSaveError),
                                        e.getMessage(), true);
                            }
                        }
                    });
                } else {
                    ActivityUtil.showAlertDialogForRequiredFields(TweetActivity.this, getString(R.string.tweetEmptyError), true);
                }
            }
        });
    }
}