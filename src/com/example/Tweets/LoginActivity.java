package com.example.Tweets;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.util.ActivityUtil;
import com.example.util.CommonUtil;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginActivity extends Activity {

    private EditText userNameEditText;
    private EditText passwordEditText;
    private Button loginButton;
    private Button registerButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Parse.initialize(this, getText(R.string.applicationId).toString(), getText(R.string.clientKey).toString());

        initializeComponents();
    }

    private void initializeComponents() {
        userNameEditText = (EditText) findViewById(R.id.userNameInLoginEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordInLoginEditText);

        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(getLoginClickListener());

        registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(getRegisterClickListener());
    }

    // On click listeners
    private View.OnClickListener getLoginClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtil.isNotEmpty(userNameEditText) && CommonUtil.isNotEmpty(passwordEditText)) {
                    String userName = userNameEditText.getText().toString().trim();
                    String password = passwordEditText.getText().toString().trim();

                    ParseUser.logInInBackground(userName, password, new LogInCallback() {
                        @Override
                        public void done(ParseUser user, ParseException e) {
                            if (e == null) {
                                Toast.makeText(LoginActivity.this, R.string.loginSuccess, Toast.LENGTH_SHORT).show();

                                Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(homeIntent);
                            } else {
                                ActivityUtil.showAlertDialogWithPositiveButton(LoginActivity.this, getString(R.string.loginError),
                                        e.getMessage(), true);
                            }
                        }
                    });
                } else {
                    Toast.makeText(LoginActivity.this, R.string.allFieldsRequired, Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    private View.OnClickListener getRegisterClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(registerIntent);
            }
        };
    }
}