package com.example.Tweets;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class TweetDetailViewActivity extends Activity {

    private String objectId;
    private TextView tweetObjectIdTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet_detail_view);

        Intent intent = getIntent();
        objectId = intent.getStringExtra("objectId");

        tweetObjectIdTextView = (TextView) findViewById(R.id.tweetObjectId);
        tweetObjectIdTextView.setText(objectId);
    }
}