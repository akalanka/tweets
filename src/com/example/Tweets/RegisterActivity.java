package com.example.Tweets;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.example.util.CommonUtil;
import com.parse.*;

public class RegisterActivity extends Activity {

    private EditText userNameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private Button signUpButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Parse.initialize(this, getText(R.string.applicationId).toString(), getText(R.string.clientKey).toString());

        initializeComponents();
    }

    private void initializeComponents() {
        userNameEditText = (EditText) findViewById(R.id.userNameEditText);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        signUpButton = (Button) findViewById(R.id.signUpButton);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser user = createParseUser();

                if (user != null) {
                    user.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e != null) {
                                Toast.makeText(RegisterActivity.this, R.string.signUpError, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(RegisterActivity.this, R.string.signUpSuccess, Toast.LENGTH_SHORT).show();

                                Intent homeIntent = new Intent(RegisterActivity.this, HomeActivity.class);
                                startActivity(homeIntent);
                            }
                        }
                    });
                }
            }
        });
    }

    private ParseUser createParseUser() {
        ParseUser user = null;
        if (CommonUtil.isNotEmpty(userNameEditText) && CommonUtil.isNotEmpty(emailEditText) && CommonUtil.isNotEmpty(passwordEditText)) {
            user = new ParseUser();
            user.setUsername(userNameEditText.getText().toString().trim());
            user.setEmail(emailEditText.getText().toString().trim());
            user.setPassword(passwordEditText.getText().toString().trim());
        } else {
            Toast.makeText(RegisterActivity.this, R.string.allFieldsRequired, Toast.LENGTH_SHORT).show();
        }

        return user;
    }
}