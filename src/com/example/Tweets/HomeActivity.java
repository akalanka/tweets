package com.example.Tweets;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import com.parse.*;

import java.util.List;

public class HomeActivity extends ListActivity {

    private List<ParseObject> tweetList;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Parse.initialize(this, getText(R.string.applicationId).toString(), getText(R.string.clientKey).toString());

        ParseUser user = ParseUser.getCurrentUser();
        if (user != null) {
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Tweet");
            query.orderByDescending("createdAt");
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> tweets, ParseException e) {
                    if (e == null) {
                        tweetList = tweets;

                        TweetAdapter tweetAdapter = new TweetAdapter(getListView().getContext(), tweetList);
                        setListAdapter(tweetAdapter);
                    }
                }
            });
        } else {
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_home, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newUpdate:
                Intent tweetIntent = new Intent(this, TweetActivity.class);
                startActivity(tweetIntent);
                break;
            case R.id.logout:
                ParseUser.logOut();

                Intent logoutIntent = new Intent(this, LoginActivity.class);
                startActivity(logoutIntent);
                break;
        }

        return true;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        ParseObject tweetObject = tweetList.get(position);
        String objectId = tweetObject.getObjectId();

//        Toast.makeText(getApplicationContext(), objectId, Toast.LENGTH_LONG).show();

        Intent tweetDetailIntent = new Intent(HomeActivity.this, TweetDetailViewActivity.class);
        tweetDetailIntent.putExtra("objectId", objectId);
        startActivity(tweetDetailIntent);
    }

}
