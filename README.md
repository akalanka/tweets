# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Simple android application with login, logout and some other back end operations involved. Services from parse.com are used as server side. 

### How do I get set up? ###

Project is based on Idea. Can be imported in Idea.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Areas covered in Android ###
* Views
* Click listeners
* Menus
* Dialogs
* parse